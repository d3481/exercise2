import React, { useState } from "react";
import { Text, View, StyleSheet, Picker,SafeAreaView, Button,Alert,TextInput,ScrollView,TouchableOpacity} from 'react-native';



let resultado=0;

const DropDownButton = () => {
  const [selectedValue, setSelectedValue] = useState("suma");
  const [inputs, setInputs] = useState([{key: '', value: ''}]);
    const addHandler = ()=>{
    const _inputs = [...inputs];
    _inputs.push({key: '', value: ''});
    setInputs(_inputs);
  }
  const deleteHandler = (key)=>{
    inputHandler('0',key)
    const _inputs = inputs.filter((input,index) => index != key);
    setInputs(_inputs);
  }
  const apicall=()=>{
    return fetch('https://www.random.org/integers/?num=1&min=1&max=1000&col=1&base=10&format=plain&rnd=new')
    .then((response) => response.json())
.then((json) => {
      resultado= json;
      setSelectedValue("suma");
    })
    .catch((error) => {
      console.error(error);
    });
    
  }
  const inputHandler = (text, key)=>{
    resultado=0;
    const _inputs = [...inputs];
    _inputs[key].value = text;
    _inputs[key].key   = key;
    setInputs(_inputs);
    
   
    const doubled = _inputs.map((number) => {
      
      
      if(number.value!=""){
      switch(selectedValue) {
 
      case 'suma':
        resultado = resultado +parseInt(number.value);
        break;
      
      case 'promedio':
        resultado = resultado +parseInt(number.value)/_inputs.length;
        break;
      default:
        Alert.alert("NUMBER NOT FOUND");
      }
      }
 
      
      });   
  }
  return (
    <div >
    
      <Text style={styles.container}>
      Operación
      </Text>
       <Picker
        selectedValue={selectedValue}
        onValueChange={(itemValue) => {
          setSelectedValue(itemValue)
          
          }}
      >
        <Picker.Item label="Suma" value="suma" />
        <Picker.Item label="Promedio" value="promedio" />
        <Picker.Item label="Aleatorio" value="aleatorio" />
      </Picker>
      


      
      <View style={styles.container}>
      <ScrollView style={styles.inputsContainer}>
      {inputs.map((input, key)=>(
        <View style={styles.inputContainer}>
          <TextInput placeholder={"0"} value={input.value}  onChangeText={(text)=>inputHandler(text,key)}/>
          <TouchableOpacity onPress = {()=> {
            deleteHandler(key);
            
            }}>
            <Text style={{color: "red", fontSize: 13}}>Eliminar</Text>
          </TouchableOpacity> 
        </View>
      ))}
      </ScrollView>
      <Button title="+" onPress={
        addHandler
        } />
      
         {selectedValue == "aleatorio" &&
        <Button
  onPress={() => {
    apicall()
    }}
  title="Generar Aleatorio"
  color="#841584"
  
/>
      }
       
    </View>
      <Text style={styles.container}>El resultado es {resultado}</Text>
      
    </div>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: 'white'
  },
  inputsContainer: {
    flex: 1, marginBottom: 20
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: "lightgray"
  }
})

export default DropDownButton;